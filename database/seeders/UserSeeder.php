<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'Yela',
            'role' => '1',
            'email' => 'yela@admin.com',
            'password' => bcrypt('12345678'),
        ]);
        $admin->assignRole('admin');
        
        $admin = User::create([
            'name' => 'Arbi',
            'role' => '1',
            'email' => 'arbi@admin.com',
            'password' => bcrypt('12345678')
        ]);
        $admin->assignRole('admin');

        $admin = User::create([
            'name' => 'Rena',
            'role' => '1',
            'email' => 'rena@admin.com',
            'password' => bcrypt('12345678')
        ]);
        
        $admin->assignRole('admin');

        $author = User::create([
            'name' => 'User input',
            'role' => '0',
            'email' => 'user@user.com',
            'password' => bcrypt('12345678')
        ]);

        $author->assignRole('author');
    }
}
