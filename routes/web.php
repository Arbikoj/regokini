<?php

use Illuminate\Support\Facades\Route;

use App\Http\Livewire\Homepage;

// auth google
use App\Http\Livewire\Google\GoogleAuth;



//admin
use App\Http\Livewire\Admin\Dashboard;
use App\Http\Livewire\Admin\UpdateHarga;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', Homepage::class)->name('homepage');

// auth google
// Route::get('/auth/google', [App\Http\Controllers\authGoogle::class, 'redirectToGoogle'])->name('google.login');
// Route::get('/auth/google/callback', [App\Http\Controllers\authGoogle::class, 'handleGoogleCallback'])->name('google.callback');
Route::get('/auth/google', [GoogleAuth::class, 'redirectToGoogle'])->name('google.login');
Route::get('/auth/google/callback', [GoogleAuth::class, 'handleGoogleCallback'])->name('google.callback');


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', Dashboard::class)
->name('dashboard');

Route::group(['middleware' => 'auth'], function ()
{
    Route::get('/admin/update', UpdateHarga::class)->name('admin.update');

});